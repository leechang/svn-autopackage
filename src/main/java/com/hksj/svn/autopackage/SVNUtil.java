package com.hksj.svn.autopackage;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class SVNUtil {
	private static final Logger LOG = Logger.getLogger(SVNUtil.class);
	
	private static String url = "https://192.168.16.116/svn/Huixin_p2p/cyb_new/cybFront";
	
	//private static String url = "https://192.168.16.116/svn/Huixin_p2p/cyb_new";
	
	private static SVNUtil svnUtil;
	
	private static SVNRepository repository = null;
	
	private static SVNClientManager svnClientManager = null;
	
	private SVNUtil() {
		
	}
	
	public static SVNUtil newInstance() {
		
		if(null == svnUtil) {
			svnUtil = new SVNUtil();
			init();
		}
		return svnUtil;
	}

	/**
	 * 
	*<b>Summary: </b>
	* init(初始化)
	 */
	private static void init() {
		DAVRepositoryFactory.setup();
        SVNRepositoryFactoryImpl.setup();
        FSRepositoryFactory.setup();
        
        try {
            repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
        }catch (SVNException e) {
        	LOG.error(e.getErrorMessage(), e);
        }
        // 身份验证
        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager("lichang","123456".toCharArray());
        repository.setAuthenticationManager(authManager);
        
        DefaultSVNOptions options = SVNWCUtil.createDefaultOptions(true);  
        svnClientManager = SVNClientManager.newInstance(options, authManager); 
	}
	
	public SVNClientManager  getSVNClientManager(){
		return svnClientManager;
	}
	
	/**
	 * 
	*<b>Summary: </b>
	* checkout(更新项目)
	* @param clientManager
	* @param url
	* @param revision
	* @param destPath
	* @param depth
	* @return
	* @throws SVNException
	 */
	public static long checkout(SVNClientManager clientManager, SVNURL url,  SVNRevision revision, File destPath, SVNDepth depth) throws SVNException {  
        SVNUpdateClient updateClient = clientManager.getUpdateClient();  
        updateClient.setIgnoreExternals(false);  
        return updateClient.doCheckout(url, destPath, revision, revision,depth, false);  
    }  
	
	/**
	 * 
	*<b>Summary: </b>
	* checkout(checkout)
	 * @throws SVNException 
	 */
	public void checkout(String workPath) throws SVNException {
		File wcDir = new File(workPath);
		//通过客户端管理类获得updateClient类的实例。
		SVNUpdateClient updateClient = svnClientManager.getUpdateClient();
		updateClient.setIgnoreExternals(false);
		//执行check out 操作，返回工作副本的版本号。
		long workingVersion= updateClient.doCheckout(SVNURL.parseURIEncoded(url), wcDir, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY,false);
		LOG.info("把版本："+workingVersion+" check out 到目录："+wcDir+"中。");
	}
	
	/**
	 * 
	*<b>Summary: </b>
	* update(更新)
	* @param workPath
	* @return
	* @throws SVNException
	 */
	public long update(File workPath) throws SVNException {  
		SVNUpdateClient updateClient = svnClientManager.getUpdateClient();  
        updateClient.setIgnoreExternals(false);  
        return updateClient.doUpdate(workPath, SVNRevision.HEAD, SVNDepth.INFINITY, false, false);  
    }  
	
	/**
	 * 
	*<b>Summary: </b>
	* update(更新)
	* @param workPath
	* @return
	* @throws SVNException
	 */
	public long update(String workPath) throws SVNException {  
		return update(new File(workPath));
    }
	
	/**
	 * 
	*<b>Summary: </b>
	* getCommitHistory(获取历史记录)
	* @param startRevision
	* @return
	* @throws SVNException
	 */
	public Set<String> getCommitHistory(long startRevision) throws SVNException {
		final Set<String> historys = new HashSet<String>();
		String[] targetPaths = new String[]{};
		
		repository.log(targetPaths, startRevision, -1, true, true,new ISVNLogEntryHandler() {
			
			@Override
			public void handleLogEntry(SVNLogEntry logEntry) throws SVNException {
				fillResult(logEntry);
			}
			
			public void fillResult(SVNLogEntry logEntry) {
            //getChangedPaths为提交的历史记录MAP key为文件名，value为文件详情
				historys.addAll(logEntry.getChangedPaths().keySet());
             }
		});
		
		return historys;
	}
	
}
