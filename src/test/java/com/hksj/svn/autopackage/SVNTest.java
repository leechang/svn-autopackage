package com.hksj.svn.autopackage;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNException;

public class SVNTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testChecout() throws SVNException {
		
		String workPath = "E:\\tools\\temp\\testbcd";
		
		//SVNUtil.newInstance().checkout(workPath);
		long version = SVNUtil.newInstance().update(workPath);
		System.out.println(version);
	}

	@Test
	public void testGetCommitHistory() {
		try {
			Set<String> historys = SVNUtil.newInstance().getCommitHistory(39613);
			System.out.println(historys.size());
			for(String history : historys) {
				System.out.println(history);
			}
		} catch (SVNException e) {
			e.printStackTrace();
		}
	}

}
